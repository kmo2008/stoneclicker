import "./stats.css"
import { useCookies } from 'react-cookie';
import { useEffect, useState } from "react";
function Stats(){
    const [cookie, setCookie] = useCookies(['cash','cashFromRebirth','cashAllTime','runs','upgrades','dps'
    ,'clickValue','click']);
    const [cash, setCash] = useState(0);
    const [cashFromRebirth, setCashFromRebirth] = useState(0);
    const [cashAllTime, setCashAllTime] = useState(0);
    const [runs, setRuns] = useState(0);
    const [upgrades, setUpgrades] = useState(0);
    const [dps, setDps] = useState(0);
    const [clickValue, setClickValue] = useState(0);
    const [click, setClick] = useState(0);

    useEffect(() => {
        setCash(cookie.cash)
        setCashFromRebirth(cookie.cashFromRebirth)
        setCashAllTime(cookie.cashAllTime)
        setRuns(cookie.runs)
        setUpgrades(cookie.upgrades)
        setDps(cookie.dps)
        setClickValue(cookie.clickValue)
        setClick(cookie.click)
    },[cookie])

    return(
        <div className="statsTextBox">
            <div className="title">General
            </div>
            <div className="statsTitle">
                Stones in bank: <br />
            </div>
            <div className="statsDesc">
                {cash}
            </div>
            <div className="statsTitle">
                Stones (this rebirth): <br />
            </div>
            <div className="statsDesc">
                {cashFromRebirth}
            </div>
            <div className="statsTitle">
                Stones (all time): <br />
            </div>
            <div className="statsDesc">
                {cashAllTime}
            </div>
            <div className="statsTitle">
                Runs started: <br />
            </div>
            <div className="statsDesc">
                {runs}
            </div>
            <div className="statsTitle">
                Upgrades owned: <br />
            </div>
            <div className="statsDesc">
                {upgrades}
            </div>
            <div className="statsTitle">
                DPS: <br />
            </div>
            <div className="statsDesc">
                {dps}
            </div>
            <div className="statsTitle">
                Click value: <br />
            </div>
            <div className="statsDesc">
                {clickValue}
            </div>
            <div className="statsTitle">
                Clicks: < br/>
            </div>
            <div className="statsDesc">
                {click}
            </div>
        </div>
    )
}


export default Stats;