import Stone from "./../../assets/stone.png"
import "./clicker.css"
import {useCookies} from 'react-cookie';

function Clicker() {
    const [cookie, setCookie] = useCookies(['cash', 'cashFromRebirth', 'cashAllTime', 'click', 'clickValue']);

    function clickStone() {
        setCookie("clickValue", parseInt(cookie.clickValue) || 1);
        let clickValue = parseInt(cookie.clickValue);
        setCookie("cash", parseInt(cookie.cash) + clickValue || 0);
        setCookie("cashFromRebirth", parseInt(cookie.cashFromRebirth) + clickValue || 0);
        setCookie("cashAllTime", parseInt(cookie.cashAllTime) + clickValue || 0);
        setCookie("click", parseInt(cookie.click) + 1 || 0);
    }

    return (
        <div>
            <img
                src={Stone}
                className="stone"
                onClick={() => clickStone()}
            />
        </div>
    )
}


export default Clicker;