export const upgradeTable = [
    {
        "id": 0,
        "name":"Better click 0",
        "baseValue": "1",
        "progress": 2.5
    },
    {
        "id": 1,
        "name":"Better click 1",
        "baseValue": "10", //10,25,67
        "progress": 2.5
    },
    {
        "id": 2,
        "name":"Better click 2",
        "baseValue": "100",
        "progress": 2.5
    },
    {
        "id": 3,
        "name":"Better click 3",
        "baseValue": "1000",
        "progress": 2.5
    },
    {
        "id": 4,
        "name":"Better click 4",
        "baseValue": "10000",
        "progress": 2.5
    },
    {
        "id": 5,
        "name":"Better click 5",
        "baseValue": "100000",
        "progress": 2.5
    },
    {
        "id": 6,
        "name":"Better click 6",
        "baseValue": "1000000",
        "progress": 2.5
    },
    {
        "id": 7,
        "name":"Better click 7",
        "baseValue": "10000000",
        "progress": 2.5
    },
    {
        "id": 8,
        "name":"Better click 8",
        "baseValue": "100000000",
        "progress": 2.5
    },
    {
        "id": 9,
        "name":"Better click 9",
        "baseValue": "1000000000",
        "progress": 2.5
    },
    {
        "id": 10,
        "name":"Better click 10",
        "baseValue": "10000000000",
        "progress": 2.5
    },
    {
        "id": 11,
        "name":"Better click 11",
        "baseValue": "100000000000",
        "progress": 2.5
    },
    {
        "id": 12,
        "name":"Better click 12",
        "baseValue": "1000000000000",
        "progress": 2.5
    },
    {
        "id": 13,
        "name":"Better click 13",
        "baseValue": "10000000000000",
        "progress": 2.5
    },
    {
        "id": 14,
        "name":"Better click 14",
        "baseValue": "100000000000000",
        "progress": 2.5
    },
    {
        "id": 15,
        "name":"Better click 15",
        "baseValue": "1000000000000000",
        "progress": 2.5
    },
    {
        "id": 16,
        "name":"Better click 16",
        "baseValue": "10000000000000000",
        "progress": 2.5
    },
    {
        "id": 17,
        "name":"Better click 17",
        "baseValue": "100000000000000000",
        "progress": 2.5
    },
    {
        "id": 18,
        "name":"Better click 18",
        "baseValue": "1000000000000000000",
        "progress": 2.5
    },
    {
        "id": 19,
        "name":"Better click 19",
        "baseValue": "10000000000000000000",
        "progress": 2.5
    },
    {
        "id": 20,
        "name":"Better click 20",
        "baseValue": "100000000000000000000",
        "progress": 2.5
    },
    {
        "id": 21,
        "name":"Better click 21",
        "baseValue": "1000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 22,
        "name":"Better click 22",
        "baseValue": "10000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 23,
        "name":"Better click 23",
        "baseValue": "100000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 24,
        "name":"Better click 24",
        "baseValue": "1000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 25,
        "name":"Better click 25",
        "baseValue": "10000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 26,
        "name":"Better click 26",
        "baseValue": "100000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 27,
        "name":"Better click 27",
        "baseValue": "1000000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 28,
        "name":"Better click 28",
        "baseValue": "10000000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 29,
        "name":"Better click 29",
        "baseValue": "100000000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 30,
        "name":"Better click 30",
        "baseValue": "1000000000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 31,
        "name":"Better click 31",
        "baseValue": "10000000000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 32,
        "name":"Better click 32",
        "baseValue": "100000000000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 33,
        "name":"Better click 33",
        "baseValue": "1000000000000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 34,
        "name":"Better click 34",
        "baseValue": "1000000000000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 35,
        "name":"Better click 35",
        "baseValue": "10000000000000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 36,
        "name":"Better click 36",
        "baseValue": "10000000000000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 37,
        "name":"Better click 37",
        "baseValue": "100000000000000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 38,
        "name":"Better click 38",
        "baseValue": "1000000000000000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 39,
        "name":"Better click 39",
        "baseValue": "10000000000000000000000000000000000000",
        "progress": 2.5
    },
    {
        "id": 40,
        "name":"Better click 40",
        "baseValue": "1000000000000000000000000000000000000000000000000000000",
        "progress": 2.5
    }
]