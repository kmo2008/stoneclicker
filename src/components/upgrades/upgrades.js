import {upgradeTable} from "./upgradeTable"
import "./upgrades.css"
import {useCookies} from 'react-cookie';
import { useEffect, useState } from "react";

function Upgrades() {
    const [cookie, setCookie] = useCookies(['upgradesLevel']);
    const [levels, setLevels] = useState({})

    useEffect(() => {
        setCookie('upgradesLevel', {"Better click" : 1})
        setLevels(cookie.upgradesLevel)
    },[])

    return (
        <div>
            {
                upgradeTable.map(upgrade => {
                    return (
                        <div className="upgradeRow">
                            {upgrade.name}

                        </div>
                    )
                })
            }
        </div>
    )
}


export default Upgrades;