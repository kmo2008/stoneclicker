import './App.css';
import Achievments from "./components/achievments/achievments"
import Stats from "./components/stats/stats"
import Clicker from "./components/clicker/clicker"
import Upgrades from "./components/upgrades/upgrades"

function App() {
  return (
    <div className='mainPage'>
      <div className='firstColumn'>
        <div className='clickerBox'>
          <Clicker/>
        </div>
        <div className='statBox'>
          <Stats/>
        </div>
      </div>
     <div className='upgradeBox'>
       <Upgrades/>
     </div>
     <div className='achivBox'>
       <Achievments />
     </div>
    </div>
  );
}

export default App;
